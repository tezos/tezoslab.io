# Pages project for the Octez and Protocol Documentation

This GitLab project was used to serve the [GitLab
pages](https://docs.gitlab.com/ee/user/project/pages/index.html) containing the 
Octez and Protocol Documentation, which were published at https://tezos.gitlab.io/.

In Feb 2025, https://tezos.gitlab.io/ has been redirected to 
https://octez.tezos.com/docs/ so this repository only serves now to redirect
all pages to the new corresponding pages.
This is implemented by file `_redirects`, see the documentation at 
<https://docs.gitlab.com/ee/user/project/pages/redirects.html>.

Eventually, this repository could still be used to keep all the versions of 
the documentation pages, but in that case they should be placed in a 
directory other than `public/`.

## Project naming

This project is named `tezos.gitlab.io`, ensuring that the published
pages are [Group
pages](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html#gitlab-pages-default-domain-names),
and that they are thus served at the address https://tezos.gitlab.io/.

## Technical details

Note that the documentation is not built in this
project. Instead, it is built in the job `publish:documentation` of
master merge pipelines of the project
[tezos/tezos](https://gitlab.com/tezos/tezos). That job builds the
documentation as a set of static HTML pages and pushes it in the 
S3 AWS repo behind <https://octez.tezos.com/>.
It also used to publish the pages as a git
commit to this project, but for now only updates the `_redirects` file.

Anyways, the push triggers a pipeline here, which
redirects the static HTML pages.

A possible alternative would be to publish the Octez and Protocol 
documentation under a [custom domain](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/index.html)
(e.g. `octez.tezos.com`); then this repository could be removed and the
pages could be served directly from `tezos/tezos`.
